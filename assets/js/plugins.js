// Avoid `console` errors in browsers that lack a console.
(function () {
    let method;
    const noop = function () {
    };
    const methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    let length = methods.length;
    const console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Scrolling
const rootEl = document.documentElement;
const navbarEl = document.getElementById('navbar');
const navbarBurger = document.getElementById('navbar-burger');
const NAVBAR_HEIGHT = 52;
const THRESHOLD = 160;
let navbarOpen = false;
let horizon = NAVBAR_HEIGHT;
let whereYouStoppedScrolling = 0;
let scrollFactor = 0;
let currentTranslate = 0;

navbarBurger.addEventListener('click', function (el) {
    navbarOpen = !navbarOpen;

    if (navbarOpen) {
        rootEl.classList.add('bd-is-clipped-touch');
    } else {
        rootEl.classList.remove('bd-is-clipped-touch');
    }
});

function upOrDown(lastY, currentY) {
    if (currentY >= lastY) {
        return goingDown(currentY);
    }
    return goingUp(currentY);
}

function goingDown(currentY) {
    var trigger = NAVBAR_HEIGHT;
    whereYouStoppedScrolling = currentY;

    if (currentY > horizon) {
        horizon = currentY;
    }

    translateHeader(currentY, false);
}

function goingUp(currentY) {
    var trigger = 0;

    if (currentY < whereYouStoppedScrolling - NAVBAR_HEIGHT) {
        horizon = currentY + NAVBAR_HEIGHT;
    }

    translateHeader(currentY, true);
}

function constrainDelta(delta) {
    return Math.max(0, Math.min(delta, NAVBAR_HEIGHT));
}

function translateHeader(currentY, upwards) {
    // let topTranslateValue;
    let translateValue = void 0;

    if (upwards && currentTranslate === 0) {
        translateValue = 0;
    } else if (currentY <= NAVBAR_HEIGHT) {
        translateValue = currentY * -1;
    } else {
        const delta = constrainDelta(Math.abs(currentY - horizon));
        translateValue = delta - NAVBAR_HEIGHT;
    }

    if (translateValue !== currentTranslate) {
        const navbarStyle = '\n        transform: translateY(' + translateValue + 'px);\n      ';
        currentTranslate = translateValue;
        navbarEl.setAttribute('style', navbarStyle);
    }

    if (currentY > THRESHOLD * 2) {
        scrollFactor = 1;
    } else if (currentY > THRESHOLD) {
        scrollFactor = (currentY - THRESHOLD) / THRESHOLD;
    } else {
        scrollFactor = 0;
    }
}

translateHeader(window.scrollY, false);

let ticking = false;
let lastY = 0;

window.addEventListener('scroll', function () {
    let currentY = window.scrollY;

    if (!ticking) {
        window.requestAnimationFrame(function () {
            upOrDown(lastY, currentY);
            ticking = false;
            lastY = currentY;
        });
    }

    ticking = true;
});


