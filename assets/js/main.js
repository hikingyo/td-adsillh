/**
 *
 **/

document.addEventListener('DOMContentLoaded', function () {

    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                const target = $el.dataset.target;
                const $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});

$('document').ready(() => {

    /**
     * Const
     */

    const $nbPerso = $('.num-person');
    const $qteIngredients = $('.qteIngredient');

    const $addPersonButton = $('.add-person');
    const $removePersonButton = $('.remove-person');

    let numPerson = parseFloat($nbPerso.first().text()) ;

    function calculateQte(coef,  _numPerson) {
        $.each($qteIngredients, (index, elem) => {
            let qte = parseFloat($(elem).text());
            qte = qte / coef * _numPerson;
            $(elem).text(precisionRound(qte, 2));
        });
        $.each($nbPerso, (i, e) => {
            $(e).text(Math.trunc(_numPerson));
        });
    }

    /*************
     * Events
     *************/

    $addPersonButton.on('click', (e) => {
        const coef = numPerson;
        numPerson++;
        calculateQte(coef, numPerson);
    });

    $removePersonButton.on('click', () => {
        // Si nombre personne == 1, on ne fait rien
        if(numPerson === 1){
            return;
        }
        const coef = numPerson;
        numPerson--;
        calculateQte(coef, numPerson);
    })
});

function precisionRound(number, precision) {
    const factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
}
