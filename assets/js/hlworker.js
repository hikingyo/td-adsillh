/*
 * Copyleft (c) 2018
 *  ----------------------------------------------------------------------------
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  hikingyo@outlook.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Hikingyo
 *  ----------------------------------------------------------------------------
 *
 */


/**
 * ServiceWorker pour HighlightJs
 * @param event
 */
onmessage = function (event) {
    console.dir(event);
    importScripts('vendor/highlight/highlight.js');
    const result = self.hljs.highlightAuto(event.data);
    console.dir(result);
    postMessage(result.value);
};
