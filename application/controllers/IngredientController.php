<?php
/**
 * Created by PhpStorm.
 * User: hikingyo
 * Date: 26/02/18
 * Time: 06:42
 */

class IngredientController extends MY_Controller
{

	/**
	 * List all ingredients
	 */
	public function index()
	{
		$this->load->library('table');
		$this->load->model('IngredientModel');
		$ingredients = $this->IngredientModel->getAll();



		$this->renderView('ingredient/ingredient_list', $data);
	}

	public function ingredient($id = null)
	{
		$this->load->model('IngredientModel');
		$this->load->helper('form');
		$this->load->library('form_validation');


		$method = $this->input->method();

		$ingredient = new Ingredient();

		if ($method === 'get' && !is_null($id)) {
			$ingredient = $this->IngredientModel->getIngredientById($id);
		}
		elseif($method === 'post'){
			$this->form_validation->set_rules('libelle', 'Libelle', 'required');
			$this->form_validation->set_rules('unit', 'Unit', 'required');

			if($this->form_validation->run() == FALSE){
				//form error
			}
			else{
				//success
			}
		}

		$pageTitle = is_null($id)? 'Création d\'un ingrédient':"Modification d'un ingrédient";

		$data = array(
			'pageTitle' => $pageTitle,
			'ingredient' => $ingredient
		);

		$this->renderView('ingredient/ingredient', $data);
	}
}
