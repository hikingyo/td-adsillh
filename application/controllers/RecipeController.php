<?php
/**
 * Created by PhpStorm.
 * User: hikingyo
 * Date: 11/02/18
 * Time: 08:10
 */

class RecipeController extends MY_Controller
{
	public function index()
	{
		$this->load->model('RecipeModel');

		$recipe = $this->RecipeModel->getRecipeById(1, true);

		$data = array(
			'pageTitle' => "Miam !!!"
		);

		$this->renderView('recipe/main', $data);
	}

	/**
	 * get a recipe
	 *
	 * @param int $id
	 */
	public function recipe(int $id)
	{

		$this->load->model('RecipeModel');

		$recipe = $this->RecipeModel->getRecipeById($id, true);

		$data = array(
			'recipe' => $recipe
		);

		$this->renderView('recipe/recipe', $data);
	}

	public function manageRecipe(int $id = null)
	{
		$this->load->model('RecipeModel');
		$this->load->helper('form');

		$method = $this->input->method();
		// GET
		if ($method === 'get') {

		}
		// $id not nul -> get recipe to update
		// otherwise, create new one

		// POST -> create || update

		$data = array(
			'pageTitle' => 'Admin'
		);

		$this->renderView('recipe/recipe_form', $data);
	}
}
