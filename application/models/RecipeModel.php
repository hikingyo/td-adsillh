<?php
/**
 * Created by PhpStorm.
 * User: hikingyo
 * Date: 04/02/18
 * Time: 11:51
 */

/**
 * Class RecipeModel
 */
class RecipeModel extends MY_Model
{
	public function __construct()
	{
		$this->tableName = 'Recipe';
		parent::__construct();
	}

	/**
	 * @param int $id
	 * @param bool $withIngredients
	 * @return mixed
	 */
	public function getRecipeById(int $id, bool $withIngredients = false)
	{
		$query = $this->db->select('*')
			->from($this->tableName)
			->where('Recipe.id', $id);

		$result = $query->get()->row(0, 'Recipe');

		if ($withIngredients) {
			$this->load('IngredientModel');
			$ingredients = $this->IngredientModel->getIngredrientsByRecipe($id);
			foreach ($ingredients as $ingredient){
				$result->addIngredient($ingredient);
			}
		}

		return $result;
	}
}
