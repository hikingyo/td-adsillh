<?php
/**
 * Created by PhpStorm.
 * User: hikingyo
 * Date: 10/02/18
 * Time: 08:52
 */

class Recipe
{
	private $id;
	private $title;
	private $category;
	private $id_category;
	private $ingredients;
	private $preparation_time;
	private $cooking_time;

	public function __construct()
	{
		$this->ingredients = array();
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 * @return Recipe
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param mixed $title
	 * @return Recipe
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param mixed $category
	 * @return Recipe
	 */
	public function setCategory($category)
	{
		$this->category = $category;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getIngredients(): array
	{
		return $this->ingredients;
	}

	/**
	 * @param array $ingredients
	 * @return Recipe
	 */
	public function setIngredients(array $ingredients): Recipe
	{
		$this->ingredients = $ingredients;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPreparationTime()
	{
		return $this->preparation_time;
	}

	/**
	 * @param mixed $preparation_time
	 * @return Recipe
	 */
	public function setPreparationTime($preparation_time)
	{
		$this->preparation_time = $preparation_time;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCookingTime()
	{
		return $this->cooking_time;
	}

	/**
	 * @param mixed $cooking_time
	 * @return Recipe
	 */
	public function setCookingTime($cooking_time)
	{
		$this->cooking_time = $cooking_time;
		return $this;
	}


	public function addIngredient(Ingredient $ingredient)
	{
		$this->ingredients[] = $ingredient;
	}
}
