<?php
/**
 * Created by PhpStorm.
 * User: hikingyo
 * Date: 10/02/18
 * Time: 08:58
 */

class Ingredient
{
	private $id;
	private $libelle;
	private $quantity;
	private $unit;
	private $recipe_id;
	private $ingredient_id;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 * @return Ingredient
	 */
	public function setId($id): Ingredient
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLibelle()
	{
		return $this->libelle;
	}

	/**
	 * @param mixed $libelle
	 * @return Ingredient
	 */
	public function setLibelle($libelle): Ingredient
	{
		$this->libelle = $libelle;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * @param mixed $quantity
	 * @return Ingredient
	 */
	public function setQuantity($quantity): Ingredient
	{
		$this->quantity = $quantity;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUnit()
	{
		return $this->unit;
	}

	/**
	 * @param mixed $unit
	 * @return Ingredient
	 */
	public function setUnit(String $unit): Ingredient
	{
		$this->unit = $unit;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getRecipeId()
	{
		return $this->recipe_id;
	}

	/**
	 * @param mixed $recipe_id
	 * @return Ingredient
	 */
	public function setRecipeId(int $recipe_id): Ingredient
	{
		$this->recipe_id = $recipe_id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIngredientId()
	{
		return $this->ingredient_id;
	}

	/**
	 * @param mixed $ingredient_id
	 * @return Ingredient
	 */
	public function setIngredientId(int $ingredient_id)
	{
		$this->ingredient_id = $ingredient_id;
		return $this;
	}



}
