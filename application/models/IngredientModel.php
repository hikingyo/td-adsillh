<?php
/**
 * Created by PhpStorm.
 * User: hikingyo
 * Date: 11/02/18
 * Time: 06:20
 */

class IngredientModel extends MY_Model
{
	public function __construct()
	{
		$this->tableName = 'Ingredient';

		parent::__construct();
	}

	public function getAll(){
		$query = $this->db->select('Ingredient.*')
			->from('Ingredient');

		$result = $query->get()->result('Ingredient');

		return $result;
	}

	public function getIngredrientsByRecipe(int $recipeId)
	{
		$query = $this->db->select('Ingredient.*, Ingredient_Recipe.*')
			->from('Recipe')
			->where('Recipe.id', $recipeId)
			->join('Ingredient_Recipe', 'Recipe.id = Ingredient_Recipe.recipe_id')
			->join('Ingredient', 'Ingredient.id = Ingredient_Recipe.ingredient_id');

		$result = $query->get()->row('Ingredient');

		return $result;
	}

	public function getIngredientById(int $id){
		$query = $this->db->where('id', $id)
			->get('Ingredient');
		$result = $query->row(0, 'Ingredient');
		return $result;
	}
}
