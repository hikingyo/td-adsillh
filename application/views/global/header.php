<?php
/**
 * Created by PhpStorm.
 * User: hikingyo
 * Date: 04/02/18
 * Time: 11:24
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html class="no-js" lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php
	echo link_tag('assets/site.webmanifest', 'manifest');
	echo link_tag('assets/icon/favicon.ico', 'shortcut icon', 'image/ico');
	echo link_tag('assets/icon/favicon-32x32.png', 'shortcut icon', 'image/png');
	echo link_tag('assets/icon/favicon-16x16.png', 'shortcut icon', 'image/png');
	echo link_tag('assets/icon/apple-touch-icon.png', 'apple-touch-icon', 'image/png');
	echo link_tag('assets/css/normalize.css');
	echo link_tag('assets/css/bulma.css');
	echo link_tag('assets/css/main.css');
	?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<title><?= $siteTitle ?> : <?= $pageTitle ?></title>
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
	your browser</a> to improve your experience and security.</p>
<![endif]-->

<header>
	<nav class="navbar is-fixed-top" id="navbar" role="navigation" aria-label="main navigation">
		<div class="navbar-brand">
			<a href="#" class="navbar-item"><img src="https://loremflickr.com/400/200/food" alt="Home"></a>
		</div>
		<button class="button navbar-burger" id="navbar-burger">
			<span>Home</span>
			<span>List</span>
			<span>Manage</span>
		</button>
		<div class="navbar-menu">
			<div class="navbar-start">
				<a class="navbar-item">
					Home
				</a>
				<a class="navbar-item">
					List
				</a>
				<a class="navbar-item">
					Manage
				</a>
			</div>

			<div class="navbar-end">
				<a class="navbar-item">
					Admin
				</a>
			</div>
		</div>
	</nav>

	<section class="hero is-primary is-bold">
		<div class="hero-body">
			<div class="container">
				<h1 class="title">
					<?= $pageTitle ?>
				</h1>
				<h2 class="subtitle">
					Que du bon pour votre assiette !
				</h2>
			</div>
		</div>
	</section>
</header>
<main class="mainContent">
	<section class="hero is-light">
		<div class="container has-text-centered">
			<h1 class="title">Trouver une recette</h1>
			<form action="#">
				<div class="field has-addons has-addons-centered">
					<div class="control">
						<input class="input is-medium" type="search" placeholder="Text input">
					</div>
					<div class="control">
						<button type="submit" class=" button fa fa-search is-medium" aria-hidden="true"></button>
					</div>
				</div>
			</form>
		</div>
	</section>
