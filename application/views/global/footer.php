<?php
/**
 * Created by PhpStorm.
 * User: hikingyo
 * Date: 04/02/18
 * Time: 11:24
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
</main>
<footer class="footer">
	<div class="container">
		<div class="content has-text-centered">
			<p>Made with <span class="fa fa-heart has-text-danger" aria-hidden="true"></span>
				by your <span class="fa fa-rocket has-text-primary" aria-hidden="true"></span>
				teacher.</p>
		</div>
	</div>
</footer>
<script src="assets/js/vendor/modernizr-3.5.0.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
		integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="assets/js/vendor/prism.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>

