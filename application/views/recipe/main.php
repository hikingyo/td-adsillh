<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="section">
	<div class="container">
		<div class="columns is-centered is-multiline">
			<article class="column is-one-third">
				<img src="https://loremflickr.com/450/450/food" alt="bouffe">
				<h2 class="has-text-centered subtitle">Ma super recette !!</h2>
				<h3 class="has-text-centered notification is-warning">Catégorie</h3>
			</article>
			<article class="column is-one-third ">
				<img src="https://loremflickr.com/450/450/food" alt="bouffe">
				<h2 class="has-text-centered subtitle">Ma super recette !!</h2>
				<h3 class="has-text-centered notification is-warning">Catégorie</h3>
			</article>
			<article class="column is-one-third ">
				<img src="https://loremflickr.com/450/450/food" alt="bouffe">
				<h2 class="has-text-centered subtitle">Ma super recette !!</h2>
				<h3 class="has-text-centered notification is-warning">Catégorie</h3>
			</article>
			<article class="column is-one-third ">
				<img src="https://loremflickr.com/450/450/food" alt="bouffe">
				<h2 class="has-text-centered subtitle">Ma super recette !!</h2>
				<h3 class="has-text-centered notification is-warning">Catégorie</h3>
			</article>
			<article class="column is-one-third ">
				<img src="https://loremflickr.com/450/450/food" alt="bouffe">
				<h2 class="has-text-centered subtitle">Ma super recette !!</h2>
				<h3 class="has-text-centered notification is-warning">Catégorie</h3>
			</article>
			<article class="column is-one-third">
				<img src="https://loremflickr.com/450/450/food" alt="bouffe">
				<h2 class="has-text-centered subtitle">Ma super recette !!</h2>
				<h3 class="has-text-centered notification is-warning">Catégorie</h3>
			</article>
		</div>
	</div>

</section>
