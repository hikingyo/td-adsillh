<?php
/**
 * Created by PhpStorm.
 * User: hikingyo
 * Date: 26/02/18
 * Time: 20:47
 */

echo form_open();
?>
<section class="section">
	<div class="container has-text-centered">
		<h1 class="title"><?php echo $pageTitle; ?></h1>
	</div>
	<div class="container box">
		<div class="field">
			<label for="libelle" class="label">Libellé</label>
			<div class="control">
				<input type="text" name="libelle"
					   value="<?php echo set_value('libelle', $ingredient->getLibelle()); ?>">
			</div>
			<p class="help is-danger"><?php echo form_error('libelle'); ?></p>
		</div>

		<div class="field">
			<label for="unit" class="label">Unité</label>
			<div class="control">
				<div class="select">
					<?php
					$unitOptions = array(
						''   => 'aucune',
						'cc' => 'cuillère à café',
						'cs' => 'cuillère à soupe',
						'cl' => 'centilitre',
						'dl' => 'decilitre',
						'l' => 'litre',
						'g'  => 'gramme',
						'kg' => 'kilogramme',
					);
					echo form_dropdown('unit', $unitOptions, $ingredient->getUnit());
					?>
				</div>

			</div>
			<p class="help is-danger"><?php echo form_error('unit'); ?></p>
		</div>

		<div class="field is-grouped is-grouped-centered">
			<div class="control">
				<button class="button is-primary">Valider</button>
			</div>
		</div>
	</div>
</section>
</form>

