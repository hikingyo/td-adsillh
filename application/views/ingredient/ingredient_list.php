<?php
/**
 * Created by PhpStorm.
 * User: hikingyo
 * Date: 26/02/18
 * Time: 20:56
 */

?>
<section class="section">
	<div class="container">
		<ul>
			<?php foreach ($ingredients as $ingredient): ?>
				<li><?php echo anchor("ingredient/" . $ingredient->getId(), $ingredient->getLibelle()); ?></li>
			<?php endforeach; ?>
		</ul>
	</div>
</section>
